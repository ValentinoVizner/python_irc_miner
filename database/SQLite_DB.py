from database.DB_Conn import DatabaseConnection
from typing import Dict, List, Union
from sqlalchemy import create_engine
import sqlalchemy



engine = sqlalchemy.create_engine('sqlite:///log_data.db', echo=False)


def checkIfTableExistsBool():
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        table_name = "All_Log_Data"
        cur.execute("""
            SELECT name
            FROM sqlite_master
            WHERE type='table' AND name=?;
        """, (table_name, ))
        exists = bool(cur.fetchone())
        return exists


def insertPandasDFtoDB(df):
    df.to_sql('All_Log_Data', con=engine, if_exists='append')

    return df

def create_LogData_table() -> None: # Ovo je dobro kod razvoja u Pycharmu pogotovo, kada nemam return, onda ide None, služi za hintanje
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute('''CREATE TABLE IF NOT EXISTS All_Log_Data
                     (Channel text, Date text, UserName text, Message text)''')
        cur.execute('''PRAGMA synchronous = EXTRA''')
        cur.execute('''PRAGMA journal_mode = WAL''')


def insertIntoDB(list_of_dict):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        """
        Not recommended cur.execute(f'INSERT INTO books VALUES("{name}", "{author}", 0)'), reason is:
        ",0); DROP TABLE books; --> this is SQL INJECTION ATTACK
        """
        for line in list_of_dict:
                cur.executemany("INSERT INTO All_Log_Data (Channel, Date, UserName, Message) VALUES (?, ?, ?, ?)", [(line['Channel'], line['Date'], line['UserName'], line['Message'])])


def getMessageFromUserOnAllChannels(username):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE UserName = ?
        """, (username,))
        string_from_all_channels = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return string_from_all_channels


def getMessageFromUserOnAllChannelsKeyword(username, keyword):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE UserName = ?
        AND Message LIKE ?
        """, (username, "%"+keyword+"%" ))
        string_from_all_channels = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return string_from_all_channels


def getMessageFromUserOnSpecificChannel(username, channel):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE UserName = ?
        AND Channel = ?
        """, (username, channel,))
        string_from_specific_channel = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return string_from_specific_channel

def getMessageFromUserOnSpecificChannelKeyword(username, keyword, channel):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE UserName = ?
        AND Message LIKE ?
        AND Channel = ?
        """, (username, "%"+keyword+"%" , channel,))
        string_from_specific_channel = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return string_from_specific_channel


def getUserFromStringOnAllChannels(string):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE (1=1)
        AND Message LIKE ?
        """, ("%"+string+"%",))
        user_from_all_channels = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return user_from_all_channels


def getUserFromStringOnAllChannelsKeyword(string, keyword):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE (1=1)
        AND Message LIKE ?
        AND Message LIKE ?
        """, ("%"+string+"%", "%"+keyword+"%",))
        user_from_all_channels = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return user_from_all_channels


def getUserFromStringOnSpecificChannel(string, channel):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE (1=1)
        AND Message LIKE ?
        AND Channel = ?
        """, ("%"+string+"%", channel,))
        user_from_specific_channel = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return user_from_specific_channel


def getUserFromStringOnSpecificChannelKeyword(string, channel, keyword):
    with DatabaseConnection('log_data.db') as conn:
        cur = conn.cursor()
        cur.execute("""
        SELECT *
        FROM All_Log_Data
        WHERE (1=1)
        AND Message LIKE ?
        AND Channel = ?
        AND Message LIKE ?
        """, ("%"+string+"%", channel, "%"+keyword+"%"))
        user_from_specific_channel = [{'Channel': row[0], 'Date': row[1], 'UserName': row[2], 'Message': row[3]} for row in cur.fetchall()]  # daje listu tupla [(name, author, read), (name, author, read)...]

    return user_from_specific_channel