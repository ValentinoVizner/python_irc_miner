class RegularExpressions:
    IRC_REGEX_LOGS = r"(?P<Date>.*?(?=\.))(\.:)(?P<UserName>.+)(:\.)(?<=:\.)(?P<Message>.*)"
    IRC_REGEX_LOGS_YEAR = r"(\*\*\*\*)(.*)(?P<Year>\b(19|20)\d{2}\b)"