from database import SQLite_DB


from utils.input_functions import (
    inputs_getUserFromStringOnAllChannels, inputs_getMessageFromUserOnAllChannels,
    inputs_getUserFromStringOnSpecificChannel, inputs_getMessageFromStringOnSpecificChannel
    )

from utils.load_data import *
from utils.timer import Timer

user_choices = {
    1: lambda: inputs_getUserFromStringOnAllChannels(input_keywords=user_input_keywords),
    2: lambda: inputs_getMessageFromUserOnAllChannels(input_keywords=user_input_keywords),
    3: lambda: inputs_getUserFromStringOnSpecificChannel(input_keywords=user_input_keywords),
    4: lambda: inputs_getMessageFromStringOnSpecificChannel(input_keywords=user_input_keywords)

}

USER_CHOICE_USERNAME_or_STRING = """
Welcome to pylog_analyzer!
Please ENTER one of the following numbers to search the IRC channel logs:
    1 - Search user's Username on ALL channels.
    2 - Search user's Message on ALL channels.
    3 - Search user's Username on PARTICULAR channel.
    4 - Search user's Message on PARTICULAR channel.
    5 - Exit.
"""


USER_CHOICE_EXTRA_KEYWORD = """
Enter Y to enter extra keywords separated by comma, or press N to search without keywords:
"""


USER_CHOICE_AGAIN = """
If you want to search again from press 1 or press 2 to exit:
"""


def menu():
    if SQLite_DB.checkIfTableExistsBool() is not True:
        SQLite_DB.create_LogData_table()
        with Timer() as t:
            data = transformDataForInitalInsert()
        with Timer() as t:
            SQLite_DB.insertIntoDB(data)

    user_input_username_or_string = int(input(USER_CHOICE_USERNAME_or_STRING))

    while user_input_username_or_string != 5:
        if user_input_username_or_string in (1, 2, 3, 4):
            global user_input_keywords
            user_input_keywords = input(
                "Enter Y to enter extra keywords separated by comma, or press N to search without keywords: "
                ).upper()
            user_choices[user_input_username_or_string]()

        else:
            print('Unknown input, type again correct number without any character!')

        search_again = input(USER_CHOICE_AGAIN)

        if search_again == '1':
            user_input_username_or_string = int(input(USER_CHOICE_USERNAME_or_STRING))
        elif search_again == '2':
            break
        else:
            print('Unknown input, please enter Y or N.')
            search_again = input(USER_CHOICE_AGAIN)

    print('Thanks for using pylog_analyzer!')


if __name__ == "__main__":
    menu()
