FAZA I

Dakle, program treba raditi u Python terminalu, GUI nije potreban, ako ti se da
možemo ga kasnije ja i ti skupa napraviti oko ovoga. S tim da još nisam radio 
GUI, al nvm.

Kad se pokrene treba mi ispisati menu s bar 3 opcije,

1) Pretraži korisnika
2) Pretraži string
3) Exit

Treća opcija je samoopisiva. Pod 1) kad se otvori treba mi ponuditi dal
želim u svim channelima (dakle svi fileovi koji su u folderu) tražiti ili u 
samo specifičnom. U oba dva slučaja poslije toga me treba pitati dal želim sve 
ispisati što je taj korisnik rekao ili samo uz njegovo ime ima još neka ključna
riječ, npr. "mirko123" user + string "AMD". Nakon toga ja upišem ime usera i
program mi ispiše u terminal ono s obzirom kaj sam odabrao.

Pod 2) će me pitati string, i isto kao i pod 1) koraci, pitat će za dal sve 
channele ili specifičan, pa će me pitati dal želim još jednu ključnu riječ uz
to i onda ja upišem string i on ispiše kaj treba, uz ime usera koji je to rekao,
odnosno cijeli redak.

Usput bi bilo dobro da na kraju ispiše count koliko ima zapisa koje je izvadio 
iz logova poslije mog upita i da ima bazu svih korisnika u sqlite formatu. 
Tu bazu možeš stvoriti zasebnom skriptom .py i koja će se zasebno pokretati 
i u tom istom folderu napraviti .sqlite datoteku. Polja mogu biti ID, 
ime usera, i count koliko puta je nešto rekao.

Kada je program gotov ponono ispisuje početni menu.
Dodatne opcije ako imaš ideja stavi pod 4) u prvom meniu. Za to što radi dodaj u
dokumentaciju.

FAZA II

Doradio sam funkcije za input u utils-ima, ove koje ne uzimaju keyword sada mogu
 uzeti keyword kao argument i po njemu pretražiti, ali i dalje mogu bez njega 
 raditi po starom. To je prvi korak da se riješiš pola funkcija i umjesto 6 imaš
 3 funkcije koje mogu primati parametre.

Iduća stvar je raspisati main tako da se funkcije zovu po indeksima iz liste,
tako izbjegneš if-elif-if-elif arrow code i imaš streamlinean kod jer je samo
jedan path-of-execution. Ja sam napravio dio toga ali nije dobro. Kada sam 
skužio da sam ogranićen samim funkcijama na njih sam se bacio.

Po meni u meni.py ima malo previše pod-granjanja i to možeš izbjeći s listama 
i parametriziranim funckijama, a.k.a. ovisno ima li ili nema parametara 
funkcija će napraviti različite stvari.

