import pandas as pd
import numpy
import re
import os
import csv

from pandas.compat import StringIO
from pprint import pprint

from regular_expressions.regex_locators import RegularExpressions

"""
--Message from user--
Format: month-day-time User_name Message_Referenced_To User_Message
Mar 30 17:12:12 .:|aaron:.	anyone know a way to make KDE shutdown open applications more forcefully? or better yet, certain ones? i have a few applications which KDE times out trying to stop, making restarts/reboots initiated through the GUI take like 20 seconds. if i close them first or just "systemctl reboot", its instant
    Match whole line: (?P<Date>.*?(?=\.))(\.:)(?P<UserName>.+)(:\.)(?<=:\.)(?P<Message>.*)
                -Group Date: Date
                -Group UserName: UserName
                -Group Message: Message


--Message from user referenced to user--
Format: month-day-time User_name Message_Referenced_To User_Message
e.g. Mar 14 10:43:51 .:Stryyker:.	I guess if you're unconscious you lose time and it feels like time travel
    Regex for referenced:
        Datetime: (.+?[^\.]+)
        UserName: (?<=\.:).*(?=:\.)
        Message_Referenced_To: (?<=:\.).+(?=:)
        User_Message:

        Match whole line: (.*?(?=\.))(\.:)(.+)(:\.)(?<=:\.)(.{0,32}[^\s])(:\s)(.*)
                        -Group 1: Datetime
                        -Group 3: UserName
                        -Group 5: Message_Referenced_To
                        -Group 7: User_Message

--Get year of logging and append to Datetime--
Format: (\*\*\*\*)(.*)(?P<Year>\b(19|20)\d{2}\b)
e.g.**** BEGIN LOGGING AT Sat Mar 30 19:10:23 2019
"""




def getLogFilesFromDir():
    cwd = os.getcwd()
    log_file_path = [os.path.join(root, name)
                for root, dirs, files in os.walk(cwd+'\\place_IRClog_files_here')
                for name in files
                if name.endswith((".log"))]

    return log_file_path


def getLogFileName(path):
    return os.path.basename(path).split(".")[0].strip('#').lower()


def loadRawLogFile(log_file_path):
    with open(log_file_path, encoding="utf8") as log_file:
        log_file_list = [line.strip().split('\t') for line in log_file]

    return log_file_list


list_of_dict = []
def findIRCUserAndMssgWithExpression(log_file_path):
    with open(log_file_path,'r', encoding="utf8") as log_file:
        for line in log_file:
            log_lines = line.strip().split('\t')
            line_matches_expression = re.match(RegularExpressions.IRC_REGEX_LOGS, "".join(log_lines))
            year_matches_expression = re.match(RegularExpressions.IRC_REGEX_LOGS_YEAR, "".join(log_lines))
            if year_matches_expression:
                year = year_matches_expression.group('Year')
            if line_matches_expression:
                matching_data = {
                    'Channel': getLogFileName(log_file_path),
                    'Date': line_matches_expression.group('Date') + year,
                    'UserName': line_matches_expression.group('UserName'),
                    'Message': line_matches_expression.group('Message')
                }
                list_of_dict.append(matching_data)

    return list_of_dict


def transformToPandasDF(list_of_dict):
    df = pd.DataFrame(list_of_dict)
    return df


def transformDataForInitalInsert():
    for match in getLogFilesFromDir():
        findIRCUserAndMssgWithExpression(match)

    return list_of_dict

