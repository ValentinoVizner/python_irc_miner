import itertools
from database import SQLite_DB
from collections import defaultdict


def inputs_getUserFromStringOnAllChannels(input_keywords):
    input_string = input("Enter string: ")

    if input_keywords == 'Y':
        input_keyword_list = input("Enter keyword or keywords separated by comma: ")
        keyword_list = [string.strip() for string in input_keyword_list.split(',')]
        final_list_from_inputs = [SQLite_DB.getUserFromStringOnAllChannelsKeyword(input_string, keyword) for keyword in keyword_list]
        for user in list(itertools.chain(*final_list_from_inputs)):
            print(f"On channel: {user['Channel']}\n User: {user['UserName']} wrote:\n  {user['Message']}")
    elif input_keywords == 'N':
        final_list_from_inputs = SQLite_DB.getUserFromStringOnAllChannels(input_string)
        for user in list(itertools.chain(final_list_from_inputs)):
            print(f"On channel: {user['Channel']}\n User: {user['UserName']} wrote:\n  {user['Message']}")


def inputs_getMessageFromUserOnAllChannels(input_keywords):
    username = input("Enter the User's username: ")

    if input_keywords == 'Y':
        input_keyword_list = input("Enter keyword or keywords separated by comma: ")
        keyword_list = [string.strip() for string in input_keyword_list.split(',')]
        final_list_from_inputs = [SQLite_DB.getMessageFromUserOnAllChannelsKeyword(username, keyword) for keyword in keyword_list]
        unique_keys_dict = defaultdict(list)
        for user in list(itertools.chain(*final_list_from_inputs)):
            unique_keys_dict[user['UserName']].append(user['Message'])

        for channel, messages in unique_keys_dict.items():
            print(f"User: {username}\n Wrote next messages on all channels: ")
            [print(f"    - {message}") for message in messages]

    elif input_keywords == 'N':
        final_list_from_inputs = SQLite_DB.getMessageFromUserOnAllChannels(username)
        unique_keys_dict = defaultdict(list)
        for user in list(itertools.chain(final_list_from_inputs)):
            unique_keys_dict[user['UserName']].append(user['Message'])

        for channel, messages in unique_keys_dict.items():
            print(f"User: {username}\n Wrote next messages on all channels: ")
            [print(f"    - {message}") for message in messages]


def inputs_getMessageFromStringOnSpecificChannel(input_keywords):
    username = input("Enter the User's username: ")
    input_channel_list = input("Enter channel or a list of channels separated by comma: ")
    channel_list = [string.strip() for string in input_channel_list.split(',')]

    if input_keywords == 'Y':
        input_keyword_list = input("Enter keyword or keywords separated by comma: ")
        keyword_list = [string.strip() for string in input_keyword_list.split(',')]
        final_list_from_inputs = [SQLite_DB.getMessageFromUserOnSpecificChannelKeyword(username, keyword, channel)  for keyword in keyword_list for channel in channel_list]
    elif input_keywords == 'N':
        final_list_from_inputs = [SQLite_DB.getMessageFromUserOnSpecificChannel(username, channel) for channel in channel_list]


    unique_keys_dict = defaultdict(list)

    for user in list(itertools.chain(*final_list_from_inputs)):
        unique_keys_dict[user['Channel']].append(user['Message'])

    for channel, messages in unique_keys_dict.items():
        print(f"On channel: {channel}\n User: {username}\n Wrote messages: ")
        [print(f"    - {a}") for a in messages]


def inputs_getUserFromStringOnSpecificChannel(input_keywords):
    input_string = input("Enter the User's string/message: ")
    input_channel_list = input("Enter channel or a list of channels separated by comma: ")
    channel_list = [string.strip() for string in input_channel_list.split(',')]


    if input_keywords == 'Y':
        input_keyword_list = input("Enter keyword or keywords separated by comma: ")
        keyword_list = [string.strip() for string in input_keyword_list.split(',')]
        final_list_from_inputs = [SQLite_DB.getUserFromStringOnSpecificChannelKeyword(input_string, channel, keyword)  for keyword in keyword_list for channel in channel_list]
    elif input_keywords == 'N':
        final_list_from_inputs = [SQLite_DB.getUserFromStringOnSpecificChannel(input_string, channel) for channel in channel_list]

    unique_keys_dict = defaultdict(list)
    unique_keys_dict2 = defaultdict(list)

    for user in list(itertools.chain(*final_list_from_inputs)):
        unique_keys_dict[user['Channel']].append(user['UserName'])
        unique_keys_dict2[user['UserName']].append(user['Message'])

    for channel, username in unique_keys_dict.items():
        print("---------------------------------------")
        print(f"On channel: {channel}")
        print("---------------------------------------")

        for username, message in unique_keys_dict2.items():
            print(f"User: {username}\n  Wrote:")
            [print(f"    - {a}") for a in message]
            print("-------------------------------------------------------------------------------------------------------------------------")
